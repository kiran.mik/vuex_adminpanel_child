import Vue from 'vue';
import Vuex from 'vuex';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions'

Vue.use(Vuex);

export default new Vuex.Store({
    // state: {
    //     name: "kiran"
    // },
    // getters: {
    //     name: state => {
    //         return state.name;
    //     }
    // }
    state,
    getters,
    mutations,
    actions
});