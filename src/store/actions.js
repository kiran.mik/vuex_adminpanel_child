import axios from 'axios'
import router from '@/router'

const apiUrl="https://node.indianic.com:4069"
export default {

    // #################################### Signin #############################################

    Login({commit},data){
        axios({
            "url":apiUrl+"/api/adminLogin",
            "method":"POST",
            "headers":{
                "content-type":"application/json",
            },
            "data":JSON.stringify(data)
        }).then(res=>{
            console.log(res);
            var token = localStorage.setItem('token', res.data.access_token);
            if(res.data.status==1){
                router.push('/dashboard')
                console.log("router in store is", router.push("/dashboard"));
            }
        })
    },


    // #################################### UserListDisplay ####################################

    UserListDisplay({commit},data){
        var token = localStorage.getItem("token")
        axios({
            "url":apiUrl+"/api/userListing",
            "method":"POST",
            "headers":{
                "Content-Type":"application/json",
                "Authorization":token
            },
            "data":JSON.stringify(data)
        }).then(res=>{
            console.log("response",res);
            var userListData=res.data.data
            commit('userListData',userListData)
        })
    },

    // #################################### DeleteUser #########################################
    
    DeleteUser({commit},data){
        var token=localStorage.getItem("token");
        axios({
            "url":apiUrl+"/api/deleteUsers",
            "method":"POST",
            "headers":{
                "Content-Type":"application/json",
                "Authorization":token
            },
            "data":JSON.stringify(data)
        }).then(res=>{
            console.log('res',res);
        })
    },
    // #################################### EditUser #########################################
 
    EditUser({commit},uid){
        commit('Edituser',uid)
    },
    // #################################### submitEditUserData #########################################
 
        EditedUser({commit},userdata){
        var token=localStorage.getItem("token");
        axios({
            method:"POST",
            url:apiUrl+"/api/editUserListing",
            headers:{
                "Content-Type":"application/json",
                "Authorization":token
            },
            data:JSON.stringify(userdata)
        }).then(res=>{
            console.log(res);
        })
        router.push('/dashboard/usermanagement')

    } ,  
    // #################################### Newuser adding #########################################
    AddUser({commit},userdata){
        var token=localStorage.getItem("token");
        console.log("sdfasdf",userdata);

        axios({
            method:'POST',
            url:apiUrl+'/api/register',
            headers:{
                "Content-Type":"application/json",
                "Authorization":token
            },
            data:JSON.stringify(userdata),
        }).then(res=>{
            console.log(res);
        })
        router.push('/dashboard/usermanagement')
    }
}