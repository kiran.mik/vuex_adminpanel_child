import Vue from 'vue'
import Router from 'vue-router'
import Login from '../pages/Login'
import Dashboard from '../components/Dashboard'
import DashboardGraph from '../components/DashBoardGraph'
import Usermanagement from '../components/UserManagement/usermanage'
import NewUser from  '../components/UserManagement/NewUser'
import EditUser from '../components/UserManagement/edituser'
import CmsManagement from '../components/CmsManagement/cmsManagement'


Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/dashboard',
      component: Dashboard,
      children:[
        {
          path: '',
          component: DashboardGraph
        },
        {
          path: 'usermanagement',
          component: Usermanagement
        },
        {
          path: 'newuser',
          component: NewUser
        },
        {
          path: 'edituser',
          component: EditUser
        },{
          path: 'cmsmanagement',
          component: CmsManagement
        },
        
      ]
    }
  ]
})
